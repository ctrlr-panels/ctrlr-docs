# Ctrlr Docs / Start

* [Introduction](#introduction)
* [Ctrlr objects](objects/index.md)
* [How To](howto/index.md)

## Introduction

First to say, if you're new to development for Ctrlr, you better check
official
['Getting started' guide](https://ctrlr.org/getting-started/).
This is to ensure you have the basic understanding of what it is like to
design the Panels and the basics of MIDI. Then just play around until
you stuck with anything you have to idea how to implement. In many cases
you will come to a point when you will need to program things with
[Lua](https://www.lua.org/pil/contents.html). Lua programming basics are
beyond the scope of this document (but there could be some tips though),
but there is enough information you can find on web and there is nothing
very special.

You can find the examples of how to make use of some of the Ctrlr's
features
[at Github](https://github.com/RomanKubiak/Panels/tree/master/DEMO).

Also, as it has been said before,
[Ctrlr forum](https://ctrlr.org/forums/) is a great unsorted knowledge
base with all the widsom and experience of generations ever lived.
Please don't be lazy to check it also if you're stumped.

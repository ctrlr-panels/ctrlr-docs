# How To

[INDEX](../index.md) / How To

[How to create](#how-to-create)
* [Modal dialog window](#modal-dialog-window)
* [Popup menu](#popup-menu)

## How to create

### Modal dialog window

Here is the LUA example from
[Kiwi 3P Editor](https://ctrlr.org/kiwi-3p-editor/):

```lua
modalWindow = AlertWindow("\nCaption", "Some \nmultiline \ntext", AlertWindow.NoIcon)
modalWindow:addButton("   No   ", 0, KeyPress(KeyPress.escapeKey), KeyPress())
modalWindow:addButton("   Yes   ", 1, KeyPress(KeyPress.returnKey), KeyPress())
modalWindow:setModalHandler(windowCallback)

--  Get the button return values and enter modal state
ret = modalWindow:runModalLoop()

-- Dismiss if ‘No’ is pressed
if ret == 0 then
    return
end

-- Proceed if ‘Yes’ is pressed
if ret == 1 then
    ...
```

### Popup menu

Use [PopupMenu](../objects/index.md#popupmenu) class.

```lua
mainMenu = PopupMenu()
subMenu = PopupMenu()

mainMenu:addSectionHeader("First Section")
mainMenu:addItem(1, "Do action A", true, false, Image())
mainMenu:addItem(2, "Do action B", true, false, Image())
mainMenu:addSeparator()
mainMenu:addSectionHeader("Second Section")

subMenu:addSectionHeader("Sub Actions")
subMenu:addItem(1, "Do sub action A", true, false, Image())

mainMenu:addSubMenu("Sub actions", subMenu, true, Image(), false, 0)

result = mainMenu:show(0,0,0,0)

if result == 0 then
    return
end

if return == 1 then
   ...
end

...
```

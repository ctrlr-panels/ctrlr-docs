# Modulator Methods

[INDEX](../index.md) / [Ctrlr Objects](index.md) / Modulator Methods

* getComponent
* getLuaName
* getMaxMapped
* getMaxModulatorValue
* getMaxNonMapped
* getMidiMessage
* getMinMapped
* getMinModulatorValue
* getMinNonMapped
* getModulatorName
* getModulatorValue
* getName
* getObjectTree
* getProperty
* getPropertyDouble
* getPropertyInt
* getPropertyString
* getRestoreState
* getValue
* getValueMapped
* getValueNonMapped
* getVstIndex
* isRestoring
* removeProperty
* setModulatorValue
* setProperty
* setPropertyColour
* setPropertyDouble
* setPropertyInt
* setPropertyString
* setRestoreState
* setValue
* setValueMapped
* setValueNonMapped

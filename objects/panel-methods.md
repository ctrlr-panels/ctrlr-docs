# Panel Methods

[INDEX](../index.md) / [Ctrlr Objects](index.md) / Panel Methods

* dumpDebugData
* getBootstrapState
* getButton
* getButtonComponent
* getCanvas
* getCombo
* getComboComponent
* getComponent
* getCtrlrMIDILibrary (deprecated?)
* getFileListBox
* getFileListBoxComponent
* getFixedImageSlider
* getFixedImageSliderComponent
* getFixedSlider
* getFixedSliderComponent
* getGlobalVariable
* getImageButton
* getImageButtonComponent
* getImageSlider
* getImageSliderComponent
* getInputComparator
* getLabel
* getLabelComponent
* getLCDLabel
* getLCDLabelComponent
* getLibrary (deprecated?)
* getListBox
* getListBoxComponent
* getModulator
* getModulatorByIndex
* [getModulatorByName](#getmodulatorbyname)
* getModulatorValuesAsData
* getModulatorWithProperty
* getModulatorsWildcard
* getModulatorsWithProperty
* getNumModulators
* getObjectTree
* getPanelEditor
* getProgramState
* getProperty
* getPropertyDouble
* getPropertyInt
* getPropertyString
* getRestoreState
* getSlider
* getSliderComponent
* getToggleButton
* getToggleButtonComponent
* getWaveform
* getWaveformComponent
* isRestoring
* removeProperty
* sendMidi
* sendMidiMessageNow
* setGlobalVariable
* setModulatorValuesFromData
* setProgramState
* [setProperty](#setproperty)
* setPropertyColour
* setPropertyDouble
* setPropertyInt
* setPropertyString
* setRestoreState

## getModulatorByName

Finds modulator by name, returns `nil` if not found.

**Signature:**

```lua
getModulatorValue(name)
```

* `name` -- name of the modulator

**Example:**

```lua
mod = panel:getModulatorByName("vcf-cutoff")
```

## setProperty

Sets the value of the Panel's property.

**Signature:**

```lua
setProperty(name, value, isUndoable)
```

* `name` (string) - name of the property
* `value` (any) - value to set
* `isUndoable` (bool) - could this action be rolled back?

**Example:**

Reset the global variables:

```lua
panel:setProperty("panelGlobalVariables", "0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0:0", false)
```

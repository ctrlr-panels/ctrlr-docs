# Popup Menu Methods

[INDEX](../index.md) / [Ctrlr Objects](index.md) / Popup Menu Methods

* [addItem](#additem)
* [addSectionHeader](#addsectionheader)
* [addSeparator](#addseparator)
* [addSubMenu](#addsubmenu)
* [show](#show)

## addItem

Adds new menu item.

**Signature:**

```lua
addItem(index, text, _, _, icon-image)
```

**Example:**

```lua
popup_menu:addItem(1, "First Item", true, false, Image())
```

## addSectionHeader

Adds header to the section.

**Signature:**

```lua
addSectionHeader(text)
```

**Example:**

```lua
popup_menu:addSectionHeader("New section")
```

## addSeparator

Adds separator line.

**Example:**

```lua
popup_menu:addSeparator()
```

## addSubMenu

Attaches one popup menu to another as a submenu.

**Signature:**

```lua
addSubMenu(text, sub-popup-menu, _, icon-image, _, _)
```

**Example:**

```lua
popup_menu:addSubMenu("New sub menu", other_popup_menu, true, Image(), false, 0)
```

## show

Shows the menu.

**Signature:**

```lua
show(_, _, _, _)
```

**Example:**

```lua
result = popup_menu:show(0,0,0,0)
```

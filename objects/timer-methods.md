# Timer Methods

[INDEX](../index.md) / [Ctrlr Objects](index.md) / Timer Methods

* getTimerInterval
* isRegistered
* isTimerRunning
* setCallback
* startTimer
* stopTimer

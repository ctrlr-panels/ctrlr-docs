# BigInteger Methods

[INDEX](../index.md) / [Ctrlr Objects](index.md) / BigInteger Methods

`BigInteger` represents the big integer values.

* clear
* clearBit
* compare
* compareAbsolute
* countNumberOfSetBits
* divideBy
* exponentModulo
* findGreatestCommonDivisor
* findNextClearBit
* findNextSetBit
* getBitRange
* getBitRangeAsInt
* getHighestBit
* insertBit
* inverseModulo
* isNegative
* isOne
* isZero
* loadFromMemoryBlock
* negate
* parseString
* setBit
* setBitRangeAsInt
* setNegative
* setRange
* shiftBits
* swapWith
* toInteger
* toMemoryBlock
* toString
* __add
* __div
* __eq
* __init
* __le
* __lt
* __mul
* __sub

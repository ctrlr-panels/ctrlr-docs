# Ctrlr Objects

[INDEX](../index.md) / Ctrlr Objects

* [Global functions](#global-functions)
* [Panel](#panel)
* [Label](#label)
* [Modulator](#modulator)
* [Component](#component)
* [Canvas](#canvas)
* [Layer](#layer)
* [Timer](#timer)
* [Utility](#utility)
* [BigInteger](#biginteger)
* [PopupMenu](#popupmenu)

## Global functions

### what

`what()` will print the methods of the object

**Example:**

```lua
what(mod)
```

### how

`how()` lists *all* [the classes](lua-classes.md) bound to Lua

**Example:**

```lua
how()
```

## Panel

Panel is the main instance, corresponds to the Ctrlr Panel.  
Panel object is available globally in Lua as `panel`.

**class:** `CtrlrPanel`

* [Properties](panel-properties.md)
* [Methods](panel-methods.md)

## Label

Label is just a text label you can place on the Panel.

**class:** `CtrlrLabel`

* Properties
* [Methods](label-methods.md)

## Modulator

Modulator is almost every component you place on the Panel.

**class:** `CtrlrModulator`

* [Properties](modulator-properties.md)
* [Methods](modulator-methods.md)

## Component

**class:** `CtrlrComponent`

* [Properties](component-properties.md)
* [Methods](component-methods.md)

## Canvas

**class:** `CtrlrPanelCanvas`

* Properties
* [Methods](canvas-methods.md)

## Layer

**class:** `CtrlrPanelCanvasLayer`

* Properties
* [Methods](layer-methods.md)

## Timer

**class:** `Timer`

* Properties
* [Methods](timer-methods.md)

## Utility

* [Utility methods](utility-methods.md)

## BigInteger

`BigInteger` represents the big integer values.

**class:** `BigInteger`

* Properties
* [Methods](biginteger-methods.md)

## PopupMenu

Represents popup menu.

* Properties
* [Methods](popup-menu-methods.md)

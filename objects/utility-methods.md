# Utility Methods

[INDEX](../index.md) / [Ctrlr Objects](index.md) / Utility Methods

* askForTextInputWindow
* getDirectoryWindow
* getMidiInputDevices
* getMidiOutputDevices
* infoWindow
* openFileWindow
* packDsiData
* questionWindow
* saveFileWindow
* unpackDsiData
* warnWindow

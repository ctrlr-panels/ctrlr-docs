# Ctrlr Docs

[Ctrlr](https://ctrlr.org/) is amazing open-source cross-platform MIDI
tool and it opens up great opportunities. It is primarily used to
control MIDI compliant hardware, and it can be used both as standalone
app and a VST plugin.  
Since the first public release in 2010, Ctrlr has become the most known
free MIDI controller software and numerous Panels for a wide variety of
gear have been developed. What it has lacked so far is the full-fledged
development suite, VCS-friendly sources and, what is most important,
decent documentation. This is not to deny that sometimes the development
experience in Ctrlr could rapidly become discouraging. You may come
accross the weird bugs or awkward UI and that can be tolerated, but if
you need to literally guess how Ctrlr objects should be treated, what's
their interface, use cases and so on, it is hard to advance. You have to
google around to clarify this or that, and it appears that the most
comprehensive source of information is the
[Ctrlr's forum](https://ctrlr.org/forums) or the source code which is
luckily available [at Github](https://github.com/RomanKubiak/ctrlr).  
So, here I'm trying to collect all the knowledge I gathered so far about
the development process in Ctrlr. Hopefully, in the end we will have
the artifact which, if not the most complete, could be the good source
for the beginners at least.  
Looking ahead, I have to say I'm not going to describe the things that
are obvious, but if I do, please be patient :innocent:  

## Where do I start?

Start [here](index.md)
